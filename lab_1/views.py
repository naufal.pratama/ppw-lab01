from django.shortcuts import render
from datetime import datetime, date

tempat_kuliah = "UI"
curr_year = int(datetime.now().strftime("%Y"))

mhs_name = 'Naufal Pratama Tanansyah' # TODO Implement this
birth_date = date(1999, 10, 21) #TODO Implement this, format (Year, Month, Date)
npm = 1706979410 # TODO Implement this
hobi = 'Main, gambar, tidur, makan'
desc = 'Mahasiswa Fasilkom UI yang alhamdulillah masih bahagia hehehe'

mhs_name1 = 'Pande Ketut Cahya Nugraha' # TODO Implement this
birth_date1 = date(1999, 4, 12) #TODO Implement this, format (Year, Month, Date)
npm1 = 1706028663 # TODO Implement this
hobi1 = 'Fandom-ing we bare bears'
desc1 = 'Dia adalah Cahya, fan berat ice bear dari we bare bears namun pada kenyataannya dia adalah panda dari film yang sama.'

mhs_name2 = 'Daffa Muhammad Rayhan' # TODO Implement this
birth_date2 = date(1999, 8, 28) #TODO Implement this, format (Year, Month, Date)
npm2 = 1706026954 # TODO Implement this
hobi2 = 'Tidur, makan'
desc2 = 'Koor SI, di PMB juga aktif jadi acara. Satu SD + SMA juga hehee'

# Create your views here.
def index(request):
    response = {
	'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'hobi': hobi,
	'desc': desc, 'tempat_kuliah': tempat_kuliah,
	'name1': mhs_name1, 'age1': calculate_age(birth_date1.year), 'npm1': npm1, 'hobi1': hobi1,
	'desc1': desc1, 'tempat_kuliah1': tempat_kuliah,
	'name2': mhs_name2, 'age2': calculate_age(birth_date2.year), 'npm2': npm2, 'hobi2': hobi2,
	'desc2': desc2, 'tempat_kuliah2': tempat_kuliah
	}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
